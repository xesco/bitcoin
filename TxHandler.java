import java.util.ArrayList;

public class TxHandler {

    private UTXOPool upool;

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        upool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        // (1)
        Transaction trans;
        outputs = tx.getOutputs();
        for (int i = 0; i < outputs.size(); i++) {
            trans = new UTXO(tx.getHash(), i)
            if(!upool.contains(trans))
                return false
        }
        // (2)
        for (Input input: tx.getInputs()) {
            if(!Crypto.verifySignaturet(trans.address, ))
                return false
        }
        return true;
    }

    /**
     * @return void
     * removes from current UTXOPool upool, the outputs in tx
     * assumes tx has been correctly validated using isValidTx()
     */
    public updatePool(Transaction tx) {

    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        ArrayList<Transaction> accTxList = new ArrayList<Transaction>();

        for (Transaction tx: possibleTxs) {
            if (isValidTx(tx)) {
                accTxList.add(tx);
                updatePool(tx);
            }
        }
        Transaction[] accTxArr = new Transaction[accTxList.size()];
        return accTxList.toArray(accTxArr);
    }
}
